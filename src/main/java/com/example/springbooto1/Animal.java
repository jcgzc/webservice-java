package com.example.springbooto1;

import lombok.Value;

@Value
public class Animal {
    String name;
    String binomialName;
    String id;
    String description;
    String conservationStatus;

}
