package com.example.springbooto1;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class AnimalEntity {
    String name;
    String binomialName;
    String id;
    String description;
    String conservationStatus;
}
