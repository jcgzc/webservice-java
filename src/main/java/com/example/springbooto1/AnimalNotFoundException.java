package com.example.springbooto1;

public class AnimalNotFoundException extends Exception{
    public AnimalNotFoundException(String id) {

        super(id);
    }
}
