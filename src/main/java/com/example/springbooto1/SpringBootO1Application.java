package com.example.springbooto1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootO1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootO1Application.class, args);
    }

}
