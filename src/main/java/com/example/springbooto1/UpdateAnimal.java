package com.example.springbooto1;

import lombok.Value;

@Value
public class UpdateAnimal {
    String name;
    String binomialName;
}
